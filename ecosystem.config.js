module.exports = {
    apps : [{
        name: 'Slots',
        script: 'dist/src/main.js',
        args: '',
        instances: 1,
        autorestart: true,
        watch: false,
        max_memory_restart: '1G',
        env: {
            NODE_ENV: 'development'
        },
        env_production: {
            NODE_ENV: 'production',
            DB_USER: 'postgres',
            DB_PASSWORD: process.env.DB_PASSWORD,
            SLACK_TOKEN: process.env.SLACK_TOKEN
        }
    }],

    deploy : {
        production : {
            user : 'root',
            host : '178.128.205.255',
            ref  : 'origin/master',
            repo : 'git@gitlab.com:sashkeer/slot-machine-backend.git',
            path : '/var/www/slot-machine-backend',
            'post-deploy' : 'npm install && npm run build && pm2 reload ecosystem.config.js --env production'
        }
    }
};
