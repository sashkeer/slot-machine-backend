const path = require('path');
const { SnakeNamingStrategy } = require('typeorm-naming-strategies');

module.exports = [
  {
    name: 'default',
    type: 'postgres',
    host: process.env.DB_HOST || 'localhost',
    port: Number(process.env.DB_PORT || 5432),
    database: process.env.DB_NAME || 'games',
    schema: process.env.DB_SCHEMA || 'slots',
    logging: false,
    synchronize: true,
    username: process.env.DB_USER || 'postgres',
    password: process.env.DB_PASSWORD || 'postgres',
    namingStrategy: new SnakeNamingStrategy(),
    entities: [
      path.resolve(__dirname, 'dist/src/**/entities/*.entity{.ts,.js}'),
    ],
  },
];
