import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserModule } from './user/user.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GameModule } from './game/game.module';
import { PayModule } from './pay/pay.module';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    UserModule,
    GameModule,
    PayModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
