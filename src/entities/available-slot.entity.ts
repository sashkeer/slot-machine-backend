import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class AvailableSlot {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  slot: number;

}
