import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Point {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  value: number;

  @Column()
  user: number;

  @Column()
  type: 'add' | 'substract';

  @CreateDateColumn()
  createDate: Date;

}
