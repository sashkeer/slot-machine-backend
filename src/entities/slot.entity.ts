import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Slot {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  icon: string;

  @Column({ nullable: true })
  name: string;

  @Column({ nullable: true })
  description: string;

}
