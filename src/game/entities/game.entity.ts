import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Game {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    array: true,
    type: 'numeric',
  })
  numbers: number[];

  @Column()
  user: number;

  @CreateDateColumn()
  createDate: Date;

}
