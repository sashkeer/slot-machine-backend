import { Controller, Get, Post, Body, Param, BadRequestException } from '@nestjs/common';
import { GameService } from './game.service';
import { CreateGameDto } from './dto/create-game.dto';
import * as _ from 'lodash';
import { SlotService } from '../shared/services/slot.service';
import { AvailableSlotService } from '../shared/services/available-slot.service';
import { Game } from './entities/game.entity';

@Controller('game')
export class GameController {

  private static winCounter: number = 0;
  private static gamesCounter: number = 0;

  constructor(
    private readonly gameService: GameService,
    private readonly slotService: SlotService,
    private readonly availableSlotService: AvailableSlotService,
  ) {}

  @Post()
  create(@Body() createGameDto: CreateGameDto) {
    return 1;
  }

  @Get('/points/:userId')
  public async getUserPoints(@Param('userId') userId: number) {
    const points = await this.gameService.getUserPoints(+userId);
    const duration = await this.gameService.getLastGameDuration(userId);
    if (points < 3 && (60 * 60 * 24 < duration || duration === undefined)) {
      await this.gameService.addPoints(userId, 3 - points);
      return 3;
    }
    return points;
  }

  @Get('/slots')
  public async findSlots(@Param('userId') userId: number) {
    return this.slotService.getSlots();
  }

  @Get('/:userId')
  public async findAll(@Param('userId') userId: number) {

    const points = await this.gameService.getUserPoints(+userId);

    if (!points) throw new BadRequestException(`У вас нет монет для розыгрыша!`);

    const availableLots = await this.availableSlotService.getAvailableSlots();
    const gameSize = 3;
    const lots = await this.slotService.getSlots();

    const game: Game = await this.gameService.generateGame({
      availableLots: availableLots.map(availableLot => availableLot.slot),
      lots: lots.map(lot => lot.id),
      gameSize,
      userId,
    });

    const isWin = _.uniq(game.numbers).length === 1;

    if (isWin) {
      await this.availableSlotService.deleteAvailable(game.numbers[0])
    }

    await this.gameService.substractPoint(userId);

    return {
      id: game.id,
      game: game.numbers,
      isWin: _.uniq(game.numbers).length === 1,
    };
  }
}
