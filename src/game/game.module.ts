import { Module } from '@nestjs/common';
import { GameService } from './game.service';
import { GameController } from './game.controller';
import { SlotService } from '../shared/services/slot.service';
import { Slot } from '../entities/slot.entity';
import { Game } from './entities/game.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AvailableSlotService } from '../shared/services/available-slot.service';
import { AvailableSlot } from '../entities/available-slot.entity';
import { Point } from '../entities/point.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Game, Slot, AvailableSlot, Point]),
  ],
  controllers: [
    GameController,
  ],
  providers: [
    GameService,
    SlotService,
    AvailableSlotService,
  ],
  exports: [
    GameService,
  ],
})
export class GameModule {}
