import { Injectable } from '@nestjs/common';

import * as _ from 'lodash';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Game } from './entities/game.entity';
import { Point } from '../entities/point.entity';
const getRandomInt = (min: number, max: number) => Math.random() * (max - min) + min;

@Injectable()
export class GameService {

  constructor (
    @InjectRepository(Game) private repo: Repository<Game>,
    @InjectRepository(Point) private repoPoint: Repository<Point>,
  ) { }

  public async substractPoint (id: number) {
    return await this.repoPoint.insert({
      user: id,
      value: 1,
      type: 'substract',
    });
  }

  public async addPoint (id: number) {
    return await this.repoPoint.insert({
      user: id,
      value: 1,
      type: 'add',
    });
  }

  public async addPoints (id: number, count: number) {
    return await this.repoPoint.insert({
      user: id,
      value: count,
      type: 'add',
    });
  }

  public async getUserPoints (id: number) {
    const { sum: addSum } = await this.repoPoint
      .createQueryBuilder('point')
      .select('COALESCE(SUM(value), 0) as sum')
      .where({
        user: id,
        type: 'add',
      }).getRawOne();

    const { sum: subSum } = await this.repoPoint
      .createQueryBuilder('point')
      .select('COALESCE(SUM(value), 0) as sum')
      .where({
        user: id,
        type: 'substract',
      }).getRawOne();

    return addSum - subSum;
  }

  public async getLastGameDuration (id: number): Promise<number> {
    const row: any = await this.repo.createQueryBuilder('game')
      .select(`EXTRACT(EPOCH FROM (NOW() - create_date)) as duration`)
      .where({
        user: id,
      })
      .orderBy('create_date', 'DESC')
      .getRawOne();

    return (row || { duration: 0 }).duration
  }

  public async generateGame (options: { availableLots: number[]; lots: number[]; gameSize: number, userId: number; }): Promise<Game> {
    const { availableLots, lots, gameSize }: any = options;

    const game = [];

    const fallbackLots = lots.filter(lot => !availableLots.includes(lot));

    if (availableLots.length === 0) return { numbers: [], id: null } as any;

    for (let counter = 0; counter < gameSize; counter++) {
      const nextIndex = Math.round(getRandomInt(0, lots.length - 1));
      game.push(lots[nextIndex]);
    }

    const isWin = _.uniq(game).length === 1;
    const hasItems = availableLots.includes(game[0]); // TODO add logic for better winning

    if (isWin && !hasItems) {
      console.log(`Sorry no available items! You will lose`);
      game[0] = fallbackLots[0] || availableLots[0];
    }

    return await this.repo.save({ numbers: game, user: options.userId });
  }
}
