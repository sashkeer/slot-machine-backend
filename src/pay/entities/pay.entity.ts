import { Column, Entity, Index, PrimaryGeneratedColumn, Unique } from 'typeorm';

@Entity()
@Unique(['orderId'])
export class Order {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  value: number;

  @Column()
  userId: number;

  @Column()
  receiverId: string;

  @Column()
  orderId: number;

  @Column({ type: 'json' })
  meta: object;
}
