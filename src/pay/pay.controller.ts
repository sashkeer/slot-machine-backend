import { Controller, Get, Post, Body, Put, Param, Delete, Logger } from '@nestjs/common';
import { PayService } from './pay.service';
import { UpdatePayDto } from './dto/update-pay.dto';
import { Order } from './entities/pay.entity';
import { plainToClass } from 'class-transformer';
import { md5 } from '../utils/md5';
import { InsertResult } from 'typeorm';
import { GameModule } from '../game/game.module';
import { GameService } from '../game/game.service';

export interface IPayNotification {
  notification_type: 'order_status_change' | 'order_status_change_test';
  app_id: number;
  user_id: number;
  receiver_id: number;
  order_id: number;
  item: string;
  item_price: string;
  sig: string;
}

const secretKey = 'FLCVzWfW3RACWMEEiDvz';

@Controller('pay')
export class PayController {

  private logger = new Logger('PayLogger');

  constructor(
    private readonly payService: PayService,
    private readonly gameService: GameService,
  ) {}

  @Post()
  public async create(@Body() createPayDto: IPayNotification) {
    this.logger.log(`New payment request ${JSON.stringify(createPayDto)}`);

    try {
      await this.payService.create(
        plainToClass(Order, {
          value: +createPayDto.item_price,
          userId: +createPayDto.user_id,
          receiverId: +createPayDto.receiver_id,
          orderId: +createPayDto.order_id,
          meta: createPayDto,
        })
      );
    } catch (e) {}

    const order: Order = await this.payService.findOne({
      where: {
        orderId: +createPayDto.order_id,
      },
    });

    const signArray = [];

    Object.keys(createPayDto)
      .sort((key1, key2) => key1 > key2 ? 1 : -1)
      .forEach((key: string) => {
        if (key !== 'sig') {
          signArray.push(`${key}=${createPayDto[key]}`);
        }
      });

    signArray.push(secretKey);

    if (createPayDto.sig !== md5(signArray.join(''))) {
      this.logger.log(`Sign not valid`);
      return {
        error: {
          error_code: 10,
          error_msg: 'Оплата прошла',
          critical: true
        }
      };
    }

    await this.gameService.addPoints(createPayDto.receiver_id, 5);

    return {
      response: {
        order_id: createPayDto.order_id,
        app_order_id: order.id,
      },
    };
  }

  @Get()
  findAll() {
    return this.payService.findAll();
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updatePayDto: UpdatePayDto) {
    return this.payService.update(+id, updatePayDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.payService.remove(+id);
  }
}
