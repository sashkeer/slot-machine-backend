import { Module } from '@nestjs/common';
import { PayService } from './pay.service';
import { PayController } from './pay.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Order } from './entities/pay.entity';
import { GameModule } from '../game/game.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Order]),
    GameModule,
  ],
  controllers: [PayController],
  providers: [
    PayService,
  ]
})
export class PayModule {}
