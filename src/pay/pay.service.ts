import { Injectable } from '@nestjs/common';
import { CreatePayDto } from './dto/create-pay.dto';
import { UpdatePayDto } from './dto/update-pay.dto';
import { Order } from './entities/pay.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Point } from '../entities/point.entity';
import { InsertResult, Repository } from 'typeorm';
import { FindOneOptions } from 'typeorm/find-options/FindOneOptions';

@Injectable()
export class PayService {

  constructor (
    @InjectRepository(Order) private repo: Repository<Order>,
  ) {}

  public async create(order: Order): Promise<InsertResult> {
    return this.repo.insert(order);
  }

  findAll() {
    return `This action returns all pay`;
  }

  public async findOne(options?: FindOneOptions<Order>): Promise<Order> {
    return await this.repo.findOne(options);
  }

  update(id: number, updatePayDto: UpdatePayDto) {
    return `This action updates a #${id} pay`;
  }

  remove(id: number) {
    return `This action removes a #${id} pay`;
  }
}
