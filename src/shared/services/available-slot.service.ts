import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { AvailableSlot } from '../../entities/available-slot.entity';

@Injectable()
export class AvailableSlotService {

  constructor (
    @InjectRepository(AvailableSlot) private repo: Repository<AvailableSlot>,
  ) { }

  public async getAvailableSlots (): Promise<AvailableSlot[]> {
    return this.repo.find();
  }

  public async deleteAvailable (slot: number) {
    const availableSlot: AvailableSlot = await this.repo.findOne({ slot });
    await this.repo.delete(availableSlot.id);
  }
}
