import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Slot } from '../../entities/slot.entity';

@Injectable()
export class SlotService {

  constructor (
    @InjectRepository(Slot) private repo: Repository<Slot>,
  ) { }

  public async getSlots (): Promise<Slot[]> {
    return this.repo.find();
  }
}
