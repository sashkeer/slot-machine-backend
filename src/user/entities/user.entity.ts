import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
@Index(['uid'])
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  uid: number;

  @Column({
    default: 0,
  })
  coins: number;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  patronymic: string;
}
