const crypto = require('crypto');

export const md5 = (payload: string) => {
  return crypto.createHash('md5')
    .update(payload)
    .digest('hex');
};
